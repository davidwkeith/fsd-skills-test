import { Component, OnInit } from '@angular/core';
import { join } from 'path';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})

/**
 * The DeviceList component.
 *
 * This is a simple DeviceListComponent that grabs all the device data
 * and displays a table.
 *
 * @class
 * @author David W. Keith <git@dwk.io>
 */
export class DeviceListComponent implements OnInit {

  /**
   * True when we are only showing recent devices
   *
   * @type {boolean}
   * @default
   */
  showRecentDevices: boolean = false;

  /**
   * true when the display is pagenated
   *
   * @type {Boolean}
   * @default
   */
  isPagenated: Boolean = false;

  /**
   * The list of devices
   *
   * @type {Array}
   * @default
   */
  devices: Array<any> = [];

  /**
   * The earlest `last_seen` date for the list of devices
   *
   * @type {Date}
   * @default
   */
  filterBeginDate: Date = new Date(0);

  /**
   * The latest `last_seen` date for the list of devices
   *
   * @type {Date}
   * @default
   */
  filterEndDate: Date = new Date();

  /**
   * The current page number
   *
   * @type {number}
   * @default
   */
  pageNumber: number = 1;

  /**
   * Simple constructor
   */
  constructor(private http: HttpClient){
  }

  /**
   * Really we should be gettting the data in a service, but there is only one data URL
   * in this simple app
   */
  ngOnInit(): void {
    this.http.get('https://d27pl0lcn7u1c9.cloudfront.net/devices.json').subscribe(data => {
      this.devices = <Array<any>>data;
    });
  }

  /**
   * The filtered list of devices.
   *
   * We always display the filtered devices, even if no filter is applied.
   * This is to keep things simple, but ideally we would detect a no filter state an just return all devices.
   *
   * @type {Array}
   * @default
   */
  get filteredDevices(): Array<any> {
    const filteredDevices = [];
    const filterEndDate = this.filterEndDate.getTime();
    const filterBeginDate = this.filterBeginDate.getTime();
    this.devices.forEach(device => {
      const lastSeenDate = (new Date(device.last_seen)).getTime();
      if (lastSeenDate < filterEndDate && lastSeenDate > filterBeginDate) {
        filteredDevices.push(device);
      }
    });
    return filteredDevices;
  }

  /**
   * The pagenated filtered list of devices.
   *
   * @type {Array}
   * @default
   */
  get pagenatedFilteredDevices(): Array<any> {
    const sliceEnd:number = this.pageNumber * 10;
    const sliceStart:number = sliceEnd - 10;
    return this.filteredDevices.slice(sliceStart, sliceEnd);
  }

  /**
   * The total number of pages
   *
   * @type {number}
   * @default
   */
  get totalPages(): number {
    return Math.ceil(this.numberOfDevices/10);
  }

  /**
   * The count of devices
   *
   * @type {number}
   * @default
   */
  get numberOfDevices(): number {
    return this.filteredDevices.length;
  }

  /**
   * Show only recenctly seen devices (last 20 days)
   */
  onShowRecentDevices() {
    this.showRecentDevices = true;
    this.filterEndDate = new Date();
    // annoyingly setDate() returns an integer, rather than a Date object, so need to re-cast to Date()
    this.filterBeginDate = new Date(new Date().setDate(this.filterEndDate.getDate()-20));
  }

  /**
   * Updates the filter begin date, accepts any string parsable by Date();
   */
  onUpdateFilterBeginDate(beginDate) {
    this.filterBeginDate = new Date(beginDate);
  }

  /**
   * Updates the filter end date, accepts any string parsable by Date();
   */
  onUpdateFilterEndDate(endDate) {
    this.filterEndDate = new Date(endDate);
  }

  /**
   * Show all records
   */
  onClearFilters() {
    this.showRecentDevices = false;
    this.filterBeginDate = new Date(0);
    this.filterEndDate = new Date();
  }

  /**
   * Decrement pageNumber when previous is clicked
   */
  onPreviousPage() {
    this.pageNumber = this.pageNumber - 1;
  }

  /**
   * Inciment pageNumber when previous is clicked
   */
  onNextPage() {
    this.pageNumber = this.pageNumber + 1;
  }

  /**
   * Switch between pagenated mode and normal.
   */
  onTogglePagenation() {
    this.isPagenated = !this.isPagenated;
  }

  /**
   * Generates the CVS from the current set of filtered devices and starts downloading it.
   *
   * Ideally this would be done in a generic manner,
   * but for now the App only has one CSV export so we are keeping it simple.
   */
  onDownloadCsv() {
    let devicesCsv = 'data:text/csv;charset=utf-8;header=present,battery,file_size,id,last_seen,params\r\n'
    const link = document.createElement('a');

    this.filteredDevices.forEach(device => {
      // assumes data is all CSV safe (sample data is)
      // otherwise escaping per RFC 4180 should be done.
      devicesCsv += [
        device.battery,
        device.file_size,
        device.id,
        device.last_seen,
        device.params ].join(',');
      devicesCsv += '\r\n';
    });

    link.setAttribute('href', encodeURI(devicesCsv));
    link.setAttribute('download', 'devices.csv');
    document.body.appendChild(link); //Firefox requires this
    link.click();
  }

}
